package org.example.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;

import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;

public class HibernateDao {
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public <T> List<T> list(Class<T> classe){
        Session session = sessionFactory.openSession();
        List<T> result = session.createCriteria(classe).list();
        session.close();
        return result;
    }

    public <T> T create(T entity){
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
        session.close();
        return entity;
    }

    public <T> T findById(Class<T> classe, Serializable id){
        Session session = this.sessionFactory.openSession();
        T entity = session.get(classe, id);
        session.close();
        return entity;
    }

    public <T> List<T> findByExample(T entity){
        Session session = this.sessionFactory.openSession();
        Example example = Example.create(entity);
        List<T> results = session.createCriteria(entity.getClass()).add(example).list();
        session.close();
        return results;
    }

    public <T> List<T> paginate(Class<T> classe, int offset, int count){
        Session session = this.sessionFactory.openSession();
        Criteria criteria = session.createCriteria(classe).setFirstResult(offset).setMaxResults(count);
        List<T> result = criteria.list();
        session.close();
        return result;
    }

    public <T> List<T> paginateWhere(T entity, int offset, int size) {
        Session session = this.sessionFactory.openSession();
        Example example = Example.create(entity).ignoreCase();
        List<T> results = session.createCriteria(entity.getClass()).add(example).setFirstResult(offset).setMaxResults(size).list();
        session.close();
        return results;
    }

    public <T> List<T> paginateWithRestriction(Class<T> tClass, Criterion[] conditions, int offset, int size){
        Session session = this.sessionFactory.openSession();
        Criteria criteria = session.createCriteria(tClass);
        for (Criterion condition : conditions) {
            criteria.add(condition);
        }
        return criteria.setFirstResult(offset).setMaxResults(size).list();
    }

    public <T> List<T> findWithRestriction(Class<T> classe, Criterion[] conditions){
        Session session = this.sessionFactory.openSession();
        Criteria criteria = session.createCriteria(classe);
        for (Criterion condition : conditions) {
            criteria.add(condition);
        }
        return criteria.list();
    }

    /*public<T> int getPageCount(int nombreParPage, Class<T> tClass){
        Session session = this.sessionFactory.openSession();
    }*/


}
