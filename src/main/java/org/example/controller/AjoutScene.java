package org.example.controller;

import org.example.dao.ConnectPostgreSQL;
import org.example.models.Acteur;
import org.example.models.Acteurs;
import org.example.models.Auteur;
import org.example.models.Plateau;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@Controller
@RequestMapping("/addscene")
public class AjoutScene {
//  /addscene/pageajouscene

    @GetMapping("/pageajoutscene")
    public String voirpage(Model model){

        ArrayList <Plateau> listeplateau = new ArrayList<>() ;
        ArrayList <Acteur> listeacteur = new ArrayList<>();
        ArrayList <Auteur> listeauteur = new ArrayList<>();

        String requete1 = "select * from plateau";
        String requete2 = "select * from auteur";
        String requete3 = "select * from acteur";
        Connection conn = null;
        Statement stat = null;
        ResultSet res =null;
        try {
            conn = ConnectPostgreSQL.get_connection();

            stat = conn.createStatement();
            res = stat.executeQuery(requete1);

            while (res.next()){
                Plateau plateau = new Plateau();
                plateau.setId(res.getInt(res.findColumn("id")));
                plateau.setNomplateau(res.getString(res.findColumn("nomplateau")));
                plateau.setPlace(res.getString(res.findColumn("place")));
                listeplateau.add(plateau);
            }

            res = null;
            res = stat.executeQuery(requete2);

            while (res.next()){
                Auteur auteur = new Auteur();
                auteur.setId(res.getInt(res.findColumn("id")));
                auteur.setNom(res.getString(res.findColumn("nom")));
                listeauteur.add(auteur);
            }

            res = null;
            res = stat.executeQuery(requete3);

            while (res.next()){
                Acteur acteurs = new Acteur();
                acteurs.setId(res.getInt(res.findColumn("id")));
                acteurs.setNom(res.getString(res.findColumn("nom")));
                listeacteur.add(acteurs);
            }

            model.addAttribute("listeplateau",listeplateau);
            model.addAttribute("listeacteur",listeacteur);
            model.addAttribute("listeauteur",listeauteur);

            System.out.println("hello");

            return "tournage/formulaire";

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if(res!=null){
                try {
                    res.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            if(stat!=null){
                try {
                    stat.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }


        }

        return "tournage/formulaire";

    }
//    /addscene/save
    @PostMapping("/save")
    public String savescene(Model model,
                            @RequestParam(name = "nom") String nom,
                            @RequestParam(name = "plateau") String plateau,
                            @RequestParam(name = "auteur") String auteur,
                            @RequestParam(name = "debut") String debut,
                            @RequestParam(name = "fin") String fin,
                            @RequestParam(name = "isany") String isany,
                            HttpServletRequest request
                            ){

        int rest = Integer.parseInt(fin) - Integer.parseInt(debut);


        int isa = Integer.parseInt(isany);

        ArrayList<Acteur> listeacteur = new ArrayList<>() ;

        String requete3 = "select * from acteur";
        Connection conn = null;
        Statement stat = null;
        ResultSet res =null;

        try {
            conn = ConnectPostgreSQL.get_connection();

            stat = conn.createStatement();

            res = stat.executeQuery("select nextval('scene_id_seq')");
            int a = 1;
            while(res.next()){
                a = res.getInt(res.findColumn("nextval"));
            }

            String requete1 = "insert into scene (id,idplateau,idauteur,nom,etat,dure,heuredebut,heurefin) values ("+a+","+plateau+","+auteur+",'"+nom+"',-1,"+rest+","+debut+","+fin+")";

            stat.executeUpdate(requete1);

            res = stat.executeQuery(requete3);

            while (res.next()){
                Acteur acteurs = new Acteur();
                acteurs.setId(res.getInt(res.findColumn("id")));
                acteurs.setNom(res.getString(res.findColumn("nom")));
                listeacteur.add(acteurs);
            }

            for(int i=0;i<listeacteur.size() ;i++){
//                System.out.println(listeacteur.get(i).getNom());
//                if(request.getParameter(listeacteur.get(i).getId()));
                String str = request.getParameter(Integer.toString(listeacteur.get(i).getId()));
                if(str!=null){
                    String requete = "insert into acteurscene (idscene,idacteur) values ("+a+","+listeacteur.get(i).getId()+")";
                    stat.executeUpdate(requete);
                }
            }


            System.out.println("hello");

            return voirpage(model);

        } catch (Exception e){
            e.printStackTrace();
        } finally {

            if(stat!=null){
                try {
                    stat.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }


        }

        return "index";

    }

    @GetMapping("/pagereglage")
    public String pagereglage(Model model){

        ArrayList <Acteur> listeacteur = new ArrayList<>();
        ArrayList <Plateau> listeplateau = new ArrayList<>();

        String requete3 = "select * from acteur";
        String requete4 = "select * from plateau";
        Connection conn = null;
        Statement stat = null;
        ResultSet res =null;
        try {
            conn = ConnectPostgreSQL.get_connection();

            stat = conn.createStatement();

            res = stat.executeQuery(requete3);

            while (res.next()){
                Acteur acteurs = new Acteur();
                acteurs.setId(res.getInt(res.findColumn("id")));
                acteurs.setNom(res.getString(res.findColumn("nom")));
                listeacteur.add(acteurs);
            }

            res = stat.executeQuery(requete4);

            while (res.next()){
                Plateau plateau = new Plateau();
                plateau.setId(res.getInt(res.findColumn("id")));
                plateau.setNomplateau(res.getString(res.findColumn("nomplateau")));
                plateau.setPlace(res.getString(res.findColumn("place")));
                listeplateau.add(plateau);
            }

            model.addAttribute("listeacteur",listeacteur);
            model.addAttribute("listeplateau",listeplateau);


            return "tournage/ajoutindisp";

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if(res!=null){
                try {
                    res.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            if(stat!=null){
                try {
                    stat.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }


        }

        return "index";
    }

    @PostMapping("/savechange")
    public String savechange(Model model,
                             @RequestParam(name = "id")String id,
                             @RequestParam(name = "date") String date ,
                             @RequestParam(name = "type") String type
    ){

        System.out.println(id);
        System.out.println(date);

        Connection conn = null;
        Statement stat = null;
        ResultSet res =null;
        try {
            conn = ConnectPostgreSQL.get_connection();

            stat = conn.createStatement();

            String requete = "";

            if(type.compareToIgnoreCase("plateau")==0){

                requete = "insert into indplateau (idplateau, dateIndispo) values ("+id+",'"+date+"')";
            } else if(type.compareToIgnoreCase("acteur")==0){
                requete = "insert into indActeur values ("+id+",'"+date+"')";
            } else {
                return "index";
            }

            stat.executeUpdate(requete);


            return pagereglage(model);

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if(res!=null){
                try {
                    res.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            if(stat!=null){
                try {
                    stat.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

        }
        return "index";
    }

    @GetMapping("/pageplateau")
    public String pageajoutplateau(Model model){ return "tournage/formulaireplateau"; }

    @PostMapping("/saveplateau")
    public String saveplateau (Model model,@RequestParam(name = "nom")String nom,@RequestParam(name = "place")String place){
        Connection conn = null;
        Statement stat = null;

        try {
            conn = ConnectPostgreSQL.get_connection();
            stat = conn.createStatement();
            String requete = "insert into plateau (nomplateau,place) values ('"+nom+"','"+place+"')";
            stat.executeUpdate(requete);
            return pageajoutplateau(model);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if(stat!=null){
                try {
                    stat.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return "index";

    }

}
