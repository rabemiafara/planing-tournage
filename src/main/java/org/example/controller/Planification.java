package org.example.controller;

import org.example.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/plan")
public class Planification {

    @GetMapping("/create")
    public String createPage(Model model){
        Fonction f = new Fonction() ;
        model.addAttribute("scenes", f.listescene() );
        return "admin/planing";
    }

    @PostMapping
    public String create(@ModelAttribute Planning planning, Model model) {
        Fonction f = new Fonction() ;
        f.insert(f.reqplaning(planning.getDatedebut(),planning.getDatefin()));
        for(int i=0;i<planning.getIdscenes().length;i++)
        {
            f.insert(f.requpscene(planning.getIdscenes()[i]));
        }
        return "admin/index";
    }

}
