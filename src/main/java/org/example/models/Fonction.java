package org.example.models;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class Fonction {

    public void insert (String sql)
    {
        Statement stmt = null ;
        Connection con = null ;
        Connectionpst c = new Connectionpst() ;
        try {
            con = c.connecterPostgres() ;
            stmt = con.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            con.close();
        }
        catch(Exception e ){
            System.out.println("exception due a la requete");
        }
    }

    public String reqplaning(Date datedebut, Date datefin)
    {
        String val ="insert into planing(datedebut,datefin) values ('"+datedebut+"','"+datefin+"')";
        return val ;
    }

    public String requpscene(String idscene)
    {
        String val ="update scene set etat ="+0+" where id="+idscene ;
        return val ;
    }

    public Scenes[] listescene ()
    {
        Statement stmt = null;
        ResultSet rs = null ;
        //String sql ="select * from employespecialite where idemploye="+idemploye+" order by id asc";
        String sql = "select * from v_scene where etat=-1 order by id asc ";
        Vector<Scenes> use = new Vector<Scenes>(50);
        Scenes[] rep = null ;
        Connection con = null ;
        Connectionpst c = new Connectionpst() ;
        int i=0 ;
        try {
            con = c.connecterPostgres() ;
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Scenes s = new Scenes() ;
                s.setId(rs.getInt(1));
                s.setIdplateau(rs.getString(2));
                s.setIdauteur(rs.getString(3));
                s.setNom(rs.getString(4));
                s.setEtat(rs.getInt(5));
                s.setDure(rs.getInt(6));
                s.setHeuredebut(rs.getInt(7));
                s.setHeurefin(rs.getInt(8));
                use.add(s) ;
            }
            rep = new Scenes[use.size()] ;
            for(i=0;i<use.size();i++)
            {
                rep[i]=use.elementAt(i);
            }
            rs.close();
            stmt.close();
        }catch ( Exception e ) {
            System.out.println("connexion failed");
        }
        return rep ;
    }

}
