package org.example.models;

import java.sql.Date;

public class Planning {

    int id ;
    Date datedebut ;
    Date datefin ;
    String[] idscenes ;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(Date datedebut) {
        this.datedebut = datedebut;
    }

    public Date getDatefin() {
        return datefin;
    }

    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public String[] getIdscenes() {
        return idscenes;
    }

    public void setIdscenes(String[] idscenes) {
        this.idscenes = idscenes;
    }
}
