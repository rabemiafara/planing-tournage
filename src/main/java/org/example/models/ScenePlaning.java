package org.example.models;


import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.properties.TextAlignment;
import org.example.Base.FonctionBase;

import javax.swing.text.StyleConstants;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.util.Vector;

public class ScenePlaning {
    int id;
    String nomAuteur;
    int dure,heurDebut,heurFin;
    Date dateScene;
    String nomScene;
    String nomPlateau;
    String place;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomAuteur() {
        return nomAuteur;
    }

    public void setNomAuteur(String nomAuteur) {
        this.nomAuteur = nomAuteur;
    }

    public int getDure() {
        return dure;
    }

    public void setDure(int dure) {
        this.dure = dure;
    }

    public int getHeurDebut() {
        return heurDebut;
    }

    public void setHeurDebut(int heurDebut) {
        this.heurDebut = heurDebut;
    }

    public int getHeurFin() {
        return heurFin;
    }

    public void setHeurFin(int heurFin) {
        this.heurFin = heurFin;
    }

    public Date getDateScene() {
        return dateScene;
    }

    public void setDateScene(Date dateScene) {
        this.dateScene = dateScene;
    }

    public String getNomScene() {
        return nomScene;
    }

    public void setNomScene(String nomScene) {
        this.nomScene = nomScene;
    }

    public String getNomPlateau() {
        return nomPlateau;
    }

    public void setNomPlateau(String nomPlateau) {
        this.nomPlateau = nomPlateau;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public ScenePlaning(int id, String nomAuteur, int dure, int heurDebut, int heurFin, String nomScene, String nomPlateau, String place) {
        this.id = id;
        this.nomAuteur = nomAuteur;
        this.dure = dure;
        this.heurDebut = heurDebut;
        this.heurFin = heurFin;
        this.nomScene = nomScene;
        this.nomPlateau = nomPlateau;
        this.place = place;
    }

    public static Date datePlaningDebut(Connection connection) throws Exception {
        String sql="select datedebut from planing order by id desc limit 1";
        Vector<Object> date=FonctionBase.all(sql,connection)[0];
        if (date==null || date.size()==0){
            throw new Exception("Entrer une planing!!");
        }
        return (Date) date.elementAt(0);
    }
    public static Date dateFinPlaning(Connection connection) throws Exception {
        String sql="select datefin from planing order by id desc limit 1";
        Vector<Object> date=FonctionBase.all(sql,connection)[0];
        if (date==null || date.size()==0){
            throw new Exception("Entrer une planing!!");
        }
        return (Date) date.elementAt(0);
    }
    public static Vector<ScenePlaning>getPlaningScene(Date scene, Connection connection) throws Exception {
        Vector<ScenePlaning>planing=new Vector<>();
        String sql="select id,nom,nomplateau,nomScene,dure,heuredebut,heurefin,place from allScene where etat=0 and date('%s') not in (select dateIndispo from indplateau where indplateau.idplateau=allScene.idplateau) and date('%s') not in (select dateIndispo from indActeur where idActeur in (select acteurscene.idacteur from acteurscene where idscene=allScene.id)) order by idplateau,  heuredebut";
        sql=String.format(sql,scene.toString(),scene.toString());
        String insert="insert into planingscene( idscene, datescene) VALUES(%s,'%s')";

        Vector<Object>[]liste= FonctionBase.all(sql,connection);
        for (int i = 0; i <liste[0].size() ; i++) {
            ScenePlaning plan=new ScenePlaning((int) liste[0].elementAt(i), (String) liste[1].elementAt(i), (int) liste[4].elementAt(i), (int) liste[5].elementAt(i), (int) liste[6].elementAt(i), (String) liste[3].elementAt(i), (String) liste[2].elementAt(i), (String) liste[7].elementAt(i));
            plan.setDateScene(scene);
            planing.add(plan);
            FonctionBase.execute(String.format(insert,plan.getId(),scene),connection);
            String update="update scene set etat=1 where id="+plan.getId();
            FonctionBase.execute(update,connection);
        }
        return planing;
    }

    public static Vector<ScenePlaning>getApoitra() throws Exception {//averina -1 aveo zay ts mety de inserena ao am planingScene
        Vector<ScenePlaning>valiny=new Vector<>();
        try (Connection connection=FonctionBase.connect()){
            Date dateDebut=datePlaningDebut(connection);
            Date dateFin=dateFinPlaning(connection);
            while (dateDebut.getDate()!=(dateFin.getDate()+1) ){
                Date f=new Date();
                f.setDate(dateDebut.getDate());
                f.setMonth(dateDebut.getMonth());
                f.setYear(dateDebut.getYear());
                Vector<ScenePlaning>dateMitovy=getPlaningScene(f,connection);
                valiny.addAll(dateMitovy);
                dateDebut.setDate(dateDebut.getDate()+1);
            }
            String update="update scene set etat=-1 where etat=0";
            FonctionBase.execute(update,connection);
        }
        return valiny;
    }

    public static void exportPdf() throws IOException {//select * from planing scene
        String path="D:\\PlaningPdf.pdf";
        PdfWriter pdfWriter=new PdfWriter(path);
        PdfDocument pdfDocument=new PdfDocument(pdfWriter);
        Document document=new Document(pdfDocument);
        pdfDocument.setDefaultPageSize(PageSize.A4);

        Paragraph text=new Paragraph("Générer Un Calendrier");
        text.setTextAlignment(TextAlignment.CENTER);

        //PdfFont font= PdfFontFactory.createFont(Font);

        document.add(text);

        document.close();
        System.out.println("pdf creeee");
    }

    public static void main(String[] args) throws IOException {
        exportPdf();
    }

}
