package org.example.models;

public class Scenes {
    int id ;
    String idplateau ;
    String idauteur ;
    String nom ;
    int etat ;
    int dure ;
    int heuredebut ;
    int heurefin ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdplateau() {
        return idplateau;
    }

    public void setIdplateau(String idplateau) {
        this.idplateau = idplateau;
    }

    public String getIdauteur() {
        return idauteur;
    }

    public void setIdauteur(String idauteur) {
        this.idauteur = idauteur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public int getDure() {
        return dure;
    }

    public void setDure(int dure) {
        this.dure = dure;
    }

    public int getHeuredebut() {
        return heuredebut;
    }

    public void setHeuredebut(int heuredebut) {
        this.heuredebut = heuredebut;
    }

    public int getHeurefin() {
        return heurefin;
    }

    public void setHeurefin(int heurefin) {
        this.heurefin = heurefin;
    }
}
