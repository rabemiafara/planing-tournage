package org.example.models;

public class Scene {

    private int id;
    private int idplateau;
    private int idauteur;
    private String nom;
    private boolean etat;
    private int dure;
    private int heuredebut;
    private int heurefin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdplateau() {
        return idplateau;
    }

    public void setIdplateau(int idplateau) {
        this.idplateau = idplateau;
    }

    public int getIdauteur() {
        return idauteur;
    }

    public void setIdauteur(int idauteur) {
        this.idauteur = idauteur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public int getDure() {
        return dure;
    }

    public void setDure(int dure) {
        this.dure = dure;
    }

    public int getHeuredebut() {
        return heuredebut;
    }

    public void setHeuredebut(int heuredebut) {
        this.heuredebut = heuredebut;
    }

    public int getHeurefin() {
        return heurefin;
    }

    public void setHeurefin(int heurefin) {
        this.heurefin = heurefin;
    }
}
