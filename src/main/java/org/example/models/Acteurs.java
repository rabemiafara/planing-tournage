package org.example.models;

import org.example.Base.FonctionBase;

import java.util.Vector;

public class Acteurs {
    String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Acteurs(String nom) {
        this.nom = nom;
    }

    public static Vector<Acteurs>listScene(int idScene) throws Exception {
        Vector<Acteurs>scene=new Vector<>();
        String sql="select nom from allActeur where idscene="+idScene;
        Vector<Object>acteur= FonctionBase.select(sql)[0];
        for (Object b : acteur){
            Acteurs acteurs1 =new Acteurs((String) b);
            scene.add(acteurs1);
        }
        return scene;
    }
}
