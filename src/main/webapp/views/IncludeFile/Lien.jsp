
<header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
        <a href="#" class="logo d-flex align-items-center">
            <img src="${pageContext.request.contextPath}/assets/img/logo.png" alt="">
            <span class="d-none d-lg-block">PLANING</span>
        </a>

    </div><!-- End Logo -->

</header><!-- End Header -->

<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link " href="${pageContext.request.contextPath}/addscene/pageplateau">
                <i class="bi bi-grid"></i>
                <span>Ajouter Plateau</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link " href="${pageContext.request.contextPath}/addscene/pageajoutscene">
                <i class="bi bi-grid"></i>
                <span>Ajouter Scene</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link " href="${pageContext.request.contextPath}/addscene/pagereglage">
                <i class="bi bi-grid"></i>
                <span>Ajouter indisponible</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link " href="${pageContext.request.contextPath}/plan/create">
                <i class="bi bi-grid"></i>
                <span>Faire un planing</span>
            </a>
        </li><!-- End Dashboard Nav -->

        <li class="nav-item">
            <a class="nav-link " href="${pageContext.request.contextPath}/annonce-admin">
                <i class="bi bi-menu-button-wide">
                </i><span>Export PDF</span>
            </a>
        </li><!-- End Components Nav -->

    </ul>

</aside><!-- End Sidebar-->