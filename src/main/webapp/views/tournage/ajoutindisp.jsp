<%@ page import="org.example.models.Acteur" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.example.models.Plateau" %>

<%
    ArrayList<Acteur> listeacteur = (ArrayList<Acteur>) request.getAttribute("listeacteur");

    ArrayList<Plateau> listeplateau = (ArrayList<Plateau>) request.getAttribute("listeplateau");
%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../IncludeFile/Header.jsp"/>
<body>

<jsp:include page="../IncludeFile/Lien.jsp"/>

<main id="main" class="main">

    <div class="pagetitle">

    </div><!-- End Page Title -->

    <section class="section">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-6 d-flex flex-column align-items-center justify-content-center">

                <div class="card mb-3">
                    <div class="card-body">

                        <div class="pt-4 pb-2">
                            <h5 class="card-title text-center pb-0 fs-4">Ajout indisponibilite</h5>
                        </div>

                        <div class="col-12">
                            <label for="yourUsername" class="form-label">choix acteur</label>
                            <div class="input-group has-validation">
                            <!-- Modal Dialog Scrollable -->
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalDialogScrollable1">
                                Acteur
                            </button>
                            <div class="modal fade" id="modalDialogScrollable1" tabindex="-1">
                                <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title">Indisponibiliter acteur</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card">
                                            <div class="card-body">

                                                <form class="row g-3 needs-validation" action="${pageContext.request.contextPath}/addscene/savechange" method="POST">
                                                    <input type="text" value="acteur" name="type" hidden="true" >
                                                    <div class="col-12">
                                                        <label for="yourUsername" class="form-label">auteur</label>
                                                        <select class="form-select" aria-label="Default select example"  name="id">
                                                            <% for(int i=0;i< listeacteur.size();i++) { Acteur act = listeacteur.get(i); %>
                                                            <option value="<%=act.getId() %>"><%=act.getNom() %></option>
                                                            <% } %>

                                                        </select>
                                                    </div>
                                                    <div class="col-12">
                                                        <label for="yourUsername" class="form-label">Date d'indisponibilite</label>
                                                        <div class="input-group has-validation">
                                                            <input type="date" name="date" class="form-control" id="yourUsername"  required>
                                                            <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <button class="btn btn-primary w-100" type="submit">Inserer</button>
                                                    </div>

                                                </form>

                                            </div>
                                          </div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" data-bs-dismiss="modal" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                                </div>
                            </div><!-- End Modal Dialog Scrollable-->
                                <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                            </div>
                        </div>

                        <br><br>

                            <div class="col-12">
                                <label for="yourUsername" class="form-label">choix plateau</label>
                                <div class="input-group has-validation">
                                <!-- Modal Dialog Scrollable -->
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalDialogScrollable">
                                    Plateau
                                </button>
                                <div class="modal fade" id="modalDialogScrollable" tabindex="-1">
                                    <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="card-body">


                                                  <form class="row g-3 needs-validation" action="${pageContext.request.contextPath}/addscene/savechange" method="POST">
                                                    <input type="text" value="plateau" name="type" hidden="true" >
                                                    <div class="col-12">
                                                        <label for="yourUsername" class="form-label">plateau</label>
                                                        <select class="form-select" aria-label="Default select example"  name="id">
                                                            <% for(int i=0;i< listeplateau.size();i++) { Plateau pla = listeplateau.get(i); %>
                                                                <option value="<%=pla.getId() %>"><%=pla.getNomplateau() %></option>
                                                            <% } %>
                                                        </select>
                                                    </div>                                                        

                                                    <div class="col-12">
                                                        <label for="yourUsername" class="form-label">date</label>
                                                        <div class="input-group has-validation">
                                                            <input type="date" name="date" class="form-control" id="yourUsername"  required>
                                                            <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                                        </div>
                                                    </div>                            
                                                    <div class="col-12">
                                                        <button class="btn btn-primary w-100" type="submit">Inserer</button>
                                                    </div>
                        
                                                </form>                                                  

                                                </div>
                                              </div>
                                        </div>

                                    </div>
                                    </div>
                                </div><!-- End Modal Dialog Scrollable-->
                                    <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                </div>
                            </div>                            
                            <br><br>
                            <br><br>



                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<jsp:include page="../IncludeFile/Footer.jsp"/>
</body>
</html>












