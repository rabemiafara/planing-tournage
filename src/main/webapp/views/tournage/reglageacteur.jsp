<%@ page import="org.example.models.Acteur" %>
<%@ page import="java.util.ArrayList" %>


<%
    ArrayList<Acteur> listeacteur = (ArrayList<Acteur>) request.getAttribute("listeacteur");
%>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../IncludeFile/Header.jsp"/>
<body>

<jsp:include page="../IncludeFile/Lien.jsp"/>

<main id="main" class="main">

    <div class="pagetitle">

    </div><!-- End Page Title -->

    <section class="section">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-6 d-flex flex-column align-items-center justify-content-center">

                <div class="card mb-3">
                    <div class="card-body">

                        <div class="pt-4 pb-2">
                            <h5 class="card-title text-center pb-0 fs-4">reglage acteur </h5>
                        </div>

                        <form class="row g-3 needs-validation" action="${pageContext.request.contextPath}/addscene/savechange" method="POST">

                            <div class="col-12">
                                <label for="yourUsername" class="form-label">auteur</label>
                                <select class="form-select" aria-label="Default select example"  name="id">
                                    <% for(int i=0;i< listeacteur.size();i++) { Acteur act = listeacteur.get(i); %>
                                        <option value="<%=act.getId() %>"><%=act.getNom() %></option>
                                    <% } %>

                                </select>
                            </div>
                            <div class="col-12">
                                <label for="yourUsername" class="form-label">Date d'indisponibilite</label>
                                <div class="input-group has-validation">
                                    <input type="date" name="date" class="form-control" id="yourUsername"  required>
                                    <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-primary w-100" type="submit">Inserer</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<jsp:include page="../IncludeFile/Footer.jsp"/>
</body>

</html>












