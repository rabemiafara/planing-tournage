<%@ page import="java.util.ArrayList" %>
<%@ page import="org.example.models.Plateau" %>
<%@ page import="org.example.models.Acteur" %>
<%@ page import="org.example.models.Auteur" %>
<%

    ArrayList<Plateau> listeplateau = (ArrayList<Plateau>) request.getAttribute("listeplateau");
    ArrayList<Acteur> listeacteur = (ArrayList<Acteur>) request.getAttribute("listeacteur");
    ArrayList<Auteur> listeauteur = (ArrayList<Auteur>) request.getAttribute("listeauteur");

%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../IncludeFile/Header.jsp"/>

<body>
<jsp:include page="../IncludeFile/Lien.jsp"/>

<main id="main" class="main">

    <div class="pagetitle">

    </div><!-- End Page Title -->

    <section class="section">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-6 d-flex flex-column align-items-center justify-content-center">

                <div class="card mb-3">
                    <div class="card-body">

                        <div class="pt-4 pb-2">
                            <h5 class="card-title text-center pb-0 fs-4">Ajout scene</h5>
                        </div>

                        <form class="row g-3 needs-validation" action="${pageContext.request.contextPath}/addscene/save" method="POST">
                            <input type="text" name="isany" value="<%=listeacteur.size() %>" hidden >
                            <div class="col-12">
                                <label class="form-label">Nom</label>
                                <div class="input-group has-validation">
                                    <input type="text" name="nom" class="form-control" required>
                                    <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                </div>
                            </div>


                            <div class="col-12">
                                <label class="form-label">Plateau</label>
                                <select class="form-select" aria-label="Default select example"  name="plateau">

                                    <% for(int i=0;i< listeplateau.size();i++) { Plateau pl = listeplateau.get(i); %>
                                    <option value="<%=pl.getId() %>"> <%=pl.getNomplateau() %> a <%=pl.getPlace() %> </option>
                                    <% } %>

                                </select>
                            </div>                            

                            <div class="col-12">
                                <label class="form-label">auteur</label>
                                <select class="form-select" aria-label="Default select example"  name="auteur">
                                    <% for(int i=0;i< listeauteur.size();i++) { Auteur aut = listeauteur.get(i); %>
                                    <option value="<%=aut.getId() %>"><%=aut.getNom() %></option>
                                    <% } %>
                                </select>
                            </div>                                                        

                            <div class="col-12">
                                <label class="form-label">heure debut</label>
                                <div class="input-group has-validation">
                                    <input type="text" name="debut" class="form-control" required>
                                    <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                </div>
                            </div>                            
                         
                            <div class="col-12">
                                <label class="form-label">heure fin</label>
                                <div class="input-group has-validation">
                                    <input type="text" name="fin" class="form-control" required>
                                    <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                </div>
                            </div>

                            <div class="col-12">
                                <label class="form-label">choix acteur</label>
                                <div class="input-group has-validation">
                                <!-- Modal Dialog Scrollable -->
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalDialogScrollable">
                                    Modal Dialog Scrollable
                                </button>
                                <div class="modal fade" id="modalDialogScrollable" tabindex="-1">
                                    <div class="modal-dialog modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title">Modal Dialog Scrollable</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card">
                                                <div class="card-body">
                                                  <h5 class="card-title">Default Table</h5>
                                                  <!-- Default Table -->
                                                  <table class="table">
                                                    <thead>
                                                      <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Nom</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                    <% for(int i=0;i< listeacteur.size();i++) { Acteur act = listeacteur.get(i); %>
                                                      <tr>
                                                        <th scope="row">1</th>
                                                        <td><%=act.getNom() %></td>
                                                        <td> <input type="checkbox" name="<%=act.getId() %>" > </td>
                                                      </tr>
                                                    <% } %>
                                                    </tbody>
                                                  </table>
                                                  <!-- End Default Table Example -->
                                                </div>
                                              </div>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" data-bs-dismiss="modal" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                    </div>
                                </div><!-- End Modal Dialog Scrollable-->
                                    <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                </div>
                            </div>                            
                            <% request.setAttribute("liste",listeacteur); %>
                            <div class="col-12">
                                <button class="btn btn-primary w-100" type="submit">Inserer</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<jsp:include page="../IncludeFile/Footer.jsp"/>
</body>

</html>












