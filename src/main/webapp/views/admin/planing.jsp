<%@ page import="java.util.List" %>
<%@ page import="org.example.models.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%
  /*  List<?> typeAnnonces = (List<?>) request.getAttribute("typeAnnonce");
    int size = typeAnnonces.size();
    Admin use = (Admin)request.getSession().getAttribute("admin") ; */
    Scenes[] use = (Scenes[]) request.getAttribute("scenes");
%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../IncludeFile/Header.jsp"/>

<body>

<jsp:include page="../IncludeFile/Lien.jsp"/>

<main id="main" class="main">

    <div class="pagetitle">

    </div><!-- End Page Title -->

    <section class="section">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-6 d-flex flex-column align-items-center justify-content-center">

                <div class="card mb-3">
                    <div class="card-body">

                        <div class="pt-4 pb-2">
                            <h5 class="card-title text-center pb-0 fs-4">Ajout Planning</h5>
                        </div>

                        <form class="row g-3 needs-validation" action="${pageContext.request.contextPath}/plan" method="POST">

                            <div class="col-12">
                                <label for="datedebut" class="form-label">Date debut</label>
                                <div class="input-group has-validation">
                                    <input type="date" name="datedebut" class="form-control" id="datedebut"  required>
                                    <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                </div>
                            </div>
                            <div class="col-12">
                                <label for="datefin" class="form-label">Date fin</label>
                                <div class="input-group has-validation">
                                    <input type="date" name="datefin" class="form-control" id="datefin"  required>
                                    <div class="invalid-feedback">Veillez bien remplir le champ.</div>
                                </div>
                            </div>
                            <br>
                            <div class="col-12">
                                <label class="form-label">Liste des scenes : </label>
                            </div>
                            <br>
                            <div class="col-12">
                                <div class="col-sm-10">
                                    <table class="table">
                                        <thead>
                                        <tr class="btn-dark">
                                            <th scope="col"></th>
                                            <th scope="col">Nomscene</th>
                                            <th scope="col">Dure</th>
                                            <th scope="col">Heure_debut</th>
                                            <th scope="col">Heure_fin</th>
                                            <th scope="col">Nom_plateau</th>
                                            <th scope="col">Nom_auteur</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <% for(int i=0;i<use.length;i++) { %>
                                        <tr>
                                            <th scope="row">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" id="gridCheck1" name="idscenes" value="<%= use[i].getId() %>" >
                                                </div>
                                            </th>
                                            <td><%= use[i].getNom() %></td>
                                            <td><%= use[i].getDure() %>h</td>
                                            <td><%= use[i].getHeuredebut() %>h</td>
                                            <td><%= use[i].getHeurefin() %>h</td>
                                            <td><%= use[i].getIdplateau() %></td>
                                            <td><%= use[i].getIdauteur() %></td>
                                        </tr>
                                        <% } %>

                                        </tbody>
                                    </table>



                                </div>
                            </div>

                            <div class="col-12">
                                <button class="btn btn-primary w-100" type="submit">Valider</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->
<jsp:include page="../IncludeFile/Footer.jsp"/>
</body>

</html>












