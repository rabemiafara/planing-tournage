<%@ page import="java.util.Vector" %>
<%@ page import="org.example.models.ScenePlaning" %>
<%@ page import="org.example.models.Acteurs" %>
<%@ page import="org.example.models.Acteurs" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Vector<ScenePlaning> dispoScenePlaning = new Vector<>();
    try {
        dispoScenePlaning = ScenePlaning.getApoitra();
    } catch (Exception e) {
        e.printStackTrace();
    }
%>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../IncludeFile/Header.jsp"/>

<body>

<jsp:include page="../IncludeFile/Lien.jsp"/>

<main id="main" class="main">

    <div class="pagetitle my-2">
        <h2 class="text-capitalize text-center">Générer un calendrier</h2>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row my-2">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">planing</th>
                    <th scope="col">Plateau</th>
                    <th scope="col">scene</th>
                    <th scope="col">Debut</th>
                    <th scope="col">Fin</th>
                    <th scope="col">Durée</th>
                    <th scope="col">Auteur</th>
                    <th scope="col">Acteur</th>
                </tr>
                </thead>
                <tbody>
                <%
                    int i=0;
                    for (ScenePlaning s : dispoScenePlaning){
                        i++;
                %>
                <tr>
                    <th scope="row"><%=i%></th>
                    <td><%=(s.getDateScene().getYear()+1900)+"-"+s.getDateScene().getMonth()+"-"+s.getDateScene().getDate()%></td>
                    <td><%=s.getPlace()%></td>
                    <td><%=s.getNomScene()%></td>
                    <td><%=s.getHeurDebut()%>h</td>
                    <td><%=s.getHeurFin()%>h</td>
                    <td><%=s.getDure()%>min</td>
                    <td><%=s.getNomAuteur()%></td>
                    <td>
                        <ul>
                            <%
                                Vector<Acteurs>acteurs= new Vector<>();
                                try {
                                    acteurs = Acteurs.listScene(s.getId());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                for (Acteurs p : acteurs){ %>
                            <li><%=p.getNom()%></li>
                            <% } %>
                        </ul>
                    </td>
                </tr>
                <% }%>

                </tbody>
            </table>
        </div>
    </section>

</main><!-- End #main -->
<jsp:include page="../IncludeFile/Footer.jsp"/>
</body>

</html>







